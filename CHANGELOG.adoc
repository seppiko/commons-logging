= Seppiko Commons Logging

== 3.3.0 2024-04-25
. Update copyright
. Update document
. Optimized code
. Fixed bug
. Update OSSRH to Maven Central

== 3.2.0 2023-10-27
. Update copyright
. Update default report datetime format is ISO instant format.
. Add new method for easy used

== 3.1.0 2023-05-10
. Fixed bugs
. Update document
. Add fluent API

== 3.0.0 2022-12-01
. Add factory
. Add service provider
. Add default implementation

== 2.0.0 2022-11-19
. Update JDK17
. Add Fluent API
. Update document
. Update Junit 5.9.1
. Rename `AbstractXXX` to `IXXX`

== 1.2.0.RELEASE 2022-05-19
. Add `AbstractLoggingExtended`
. Add `fatal` level

== 1.1.0.RELEASE 2022-01-13
. Add `isXxxxEnable`
. Remove SPI
. Remove `Manager`
. Update copyright

== 1.0.0.RELEASE 2020-11-03
. Implemented the SPI method
. Complete document
. Complete demo
