/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.test;

import org.junit.jupiter.api.Test;
import org.seppiko.commons.logging.Logging;
import org.seppiko.commons.logging.LoggingFactory;

/**
 * @author Leonard Woo
 */
public class SCLTests {

  @Test
  public void loggingTest() {
    Logging log = LoggingFactory.getLogging(this.getClass().getName() + "::loggingTest()");
    log.trace("SCL::Test1", new TestException("Found test exception."));
    log.debug("SCL::Test2", TestException::new);
    log.log(Logging.LEVEL_WARN_INT, "SCL::Test3 with int level");
    log.atError().message("SCL::BuilderTest4").log();
  }

}
