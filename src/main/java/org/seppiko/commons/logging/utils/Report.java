/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.logging.utils;

import java.io.PrintStream;

/**
 * Log report and print.
 *
 * @author Leonard Woo
 */
public class Report {

  private static final PrintStream printDiag;
  private static final PrintStream print;

  static {
    printDiag = System.err;
    print = System.out;
  }

  /**
   * print diagnostic.
   *
   * @param message diagnostic message.
   */
  public static void diag(String message) {
    diag(message, null);
  }

  /**
   * print diagnostic.
   *
   * @param message diagnostic message.
   * @param cause diagnostic throwable.
   */
  public static void diag(String message, Throwable cause) {
    printDiag.println(message);
    if (cause != null) {
      cause.printStackTrace(printDiag);
    }
    printDiag.flush();
  }

  /**
   * print message.
   *
   * @param message message.
   */
  public static void print(String message) {
    print(message, null);
  }

  /**
   * print message.
   *
   * @param message message.
   * @param cause throwable.
   */
  public static void print(String message, Throwable cause) {
    print.println(message);
    if (cause != null) {
      cause.printStackTrace(print);
      print.println();
    }
    print.flush();
  }
}
