/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.logging.utils;

import java.util.Arrays;
import java.util.Objects;
import java.util.function.Supplier;

/**
 * Default Logging Utilities.
 *
 * @author Leonard Woo
 */
public class Utils {

  /** Default constructor. */
  private Utils() {}

  /** null cause. */
  public static final Throwable NON_CAUSE = null;

  /** null object array. */
  public static final Object[] NULL_OBJECTS = null;

  /** null supplier array. */
  public static final Supplier<?>[] NULL_SUPPLIERS = null;

  /**
   * Convert {@link Supplier} array to {@link Object} array without {@code null}.
   *
   * @param suppliers the {@link Supplier} array
   * @return {@link Object} array
   */
  public static Object[] getSuppliers(Supplier<?>[] suppliers) {
    return Arrays.stream(suppliers)
        .filter(Objects::nonNull)
        .map(Supplier::get)
        .toArray(Object[]::new);
  }
}
