/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.logging;

import java.util.function.Supplier;

/**
 * Logging builder interface
 *
 * @author Leonard Woo
 */
public interface LoggingBuilder {

  /**
   * Set message.
   *
   * @param message the message object to log.
   * @return this {@link LoggingBuilder} instance.
   */
  LoggingBuilder message(CharSequence message);

  /**
   * Set throwable.
   *
   * @param cause the {@link Throwable} subclass to log, including its stack trace.
   * @return this {@link LoggingBuilder} instance.
   */
  LoggingBuilder withCause(Throwable cause);

  /**
   * Set object parameter.
   *
   * @param paramSupplier the parameters to log.
   * @return this {@link LoggingBuilder} instance.
   */
  LoggingBuilder param(Supplier<?> paramSupplier);

  /**
   * Set object parameter.
   *
   * @param paramObject the parameter objects to log.
   * @return this {@link LoggingBuilder} instance.
   */
  LoggingBuilder param(Object paramObject);

  /** Log builder. */
  void log();
}
