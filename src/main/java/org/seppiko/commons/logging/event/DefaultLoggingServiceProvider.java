/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.logging.event;

import org.seppiko.commons.logging.ILoggingFactory;
import org.seppiko.commons.logging.spi.LoggingServiceProvider;

/**
 * Default logging service provider implementation.
 *
 * @author Leonard Woo
 */
public class DefaultLoggingServiceProvider implements LoggingServiceProvider {

  /** Default constructor. */
  public DefaultLoggingServiceProvider() {}

  /** {@inheritDoc} */
  @Override
  public ILoggingFactory getILoggingFactory() {
    return getLoggingFactory();
  }

  /**
   * Get default logging factory.
   *
   * @return default logging factory.
   */
  public DefaultLoggingFactory getLoggingFactory() {
    return new DefaultLoggingFactory();
  }

  /** {@inheritDoc} */
  @Override
  public void initialize() {
    //DO NOTHING
  }

}
