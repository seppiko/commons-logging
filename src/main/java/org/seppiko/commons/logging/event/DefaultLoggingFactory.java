/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.logging.event;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.seppiko.commons.logging.ILoggingFactory;
import org.seppiko.commons.logging.Logging;

/**
 * Default Logging factory.
 *
 * @author Leonard Woo
 */
public class DefaultLoggingFactory implements ILoggingFactory {

  private final Map<String, DefaultLogging> loggingMap = new ConcurrentHashMap<>();

  /** Default factory constructor. */
  public DefaultLoggingFactory() {}

  /** {@inheritDoc} */
  @Override
  public Logging getLogging(String name) {
    return loggingMap.computeIfAbsent(name, DefaultLogging::new);
  }

  /**
   * Get logging list.
   *
   * @return logging list.
   */
  public List<DefaultLogging> getLoggingList() {
    return new ArrayList<>(loggingMap.values());
  }

  /** Clear logging list. */
  public void clear() {
    loggingMap.clear();
  }
}
