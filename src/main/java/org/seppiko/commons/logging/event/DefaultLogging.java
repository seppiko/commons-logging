/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.logging.event;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Objects;
import java.util.StringJoiner;
import java.util.function.Supplier;
import org.seppiko.commons.logging.Logging;
import org.seppiko.commons.logging.LoggingBuilder;
import org.seppiko.commons.logging.utils.Report;
import org.seppiko.commons.logging.utils.Utils;

/**
 * Default logging implementation.
 *
 * @author Leonard Woo
 */
public class DefaultLogging implements Logging {

  /** Default logging datetime format. */
  private static final DateTimeFormatter dateFormatter = DateTimeFormatter.ISO_INSTANT;
  /** Current log level. */
  protected volatile int currentLogLevel;
  /** The short name of this simple log instance. */
  private volatile String name;

  /** Default logging constructor. */
  private DefaultLogging() {}

  /**
   * Constructor with logging name.
   *
   * @param name logging name.
   */
  public DefaultLogging(String name) {
    this.name = name;
  }

  /**
   * Set delegate logging.
   *
   * @param logging delegate logging.
   */
  public void setDelegate(Logging logging) {
    name = logging.getName();
  }

  /** {@inheritDoc} */
  @Override
  public String getName() {
    return name;
  }

  /** {@inheritDoc} */
  @Override
  public boolean isEnable(int level) {
    return level >= currentLogLevel;
  }

  /** {@inheritDoc} */
  @Override
  public void log(int level, CharSequence message) {
    log(level, message, Utils.NON_CAUSE);
  }

  /** {@inheritDoc} */
  @Override
  public void log(int level, CharSequence message, Throwable cause) {
    log(level, message, cause, Utils.NULL_SUPPLIERS);
  }

  /** {@inheritDoc} */
  @Override
  public void log(int level, CharSequence message, Supplier<?>... paramSuppliers) {
    log(level, message, Utils.NON_CAUSE, paramSuppliers);
  }

  /** {@inheritDoc} */
  @Override
  public void log(int level, CharSequence message, Object... params) {
    log(level, message, Utils.NON_CAUSE, params);
  }

  /** {@inheritDoc} */
  @Override
  public void log(int level, CharSequence message, Throwable cause, Supplier<?>... paramSuppliers) {
    Object[] params = Utils.NULL_OBJECTS;
    if (paramSuppliers != null && paramSuppliers.length > 0) {
      params = Utils.getSuppliers(paramSuppliers);
    }

    log(level, message, cause, params);
  }

  /**
   * Default logging print implementation
   *
   * @param level the logging level.
   * @param message the message string to be logged.
   * @param cause a {@link Throwable} subclass or {@code null}.
   * @param params the parameter objects or {@code null} to be logged.
   */
  @Override
  public void log(int level, CharSequence message, Throwable cause, Object... params) {
    final StringBuilder builder = new StringBuilder();

    final ZonedDateTime dateTime = ZonedDateTime.now();
    builder.append(dateTime.format(dateFormatter));
    builder.append(" ");

    switch (level) {
      case LEVEL_TRACE_INT -> builder.append("[TRACE] ");
      case LEVEL_DEBUG_INT -> builder.append("[DEBUG] ");
      case LEVEL_INFO_INT  -> builder.append("[INFO]  ");
      case LEVEL_WARN_INT  -> builder.append("[WARN]  ");
      case LEVEL_ERROR_INT -> builder.append("[ERROR] ");
      case LEVEL_FATAL_INT -> builder.append("[FATAL] ");
      default -> throw new IllegalArgumentException("Unexpected level value: " + level);
    }
    builder.append(name);
    builder.append(" - ");
    builder.append(message);
    builder.append(" ");

    if (params != null && params.length > 0) {
      final StringJoiner objJoiner = new StringJoiner(", ");
      Arrays.stream(params)
          .filter(Objects::nonNull)
          .forEach(obj -> objJoiner.add(obj.toString()));

      builder.append("<");
      builder.append(objJoiner);
      builder.append(">");
    }

    Report.print(builder.toString(), cause);
  }

  /** {@inheritDoc} */
  @Override
  public boolean isTraceEnable() {
    return isEnable(LEVEL_TRACE_INT);
  }

  /** {@inheritDoc} */
  @Override
  public void trace(CharSequence message) {
    log(LEVEL_TRACE_INT, message);
  }

  /** {@inheritDoc} */
  @Override
  public void trace(CharSequence message, Throwable cause) {
    log(LEVEL_TRACE_INT, message, cause, Utils.NULL_OBJECTS);
  }

  /** {@inheritDoc} */
  @Override
  public void trace(CharSequence message, Object... params) {
    log(LEVEL_TRACE_INT, message, Utils.NON_CAUSE, params);
  }

  /** {@inheritDoc} */
  @Override
  public void trace(CharSequence message, Supplier<?>... paramsSupplier) {
    log(LEVEL_TRACE_INT, message, Utils.NON_CAUSE, paramsSupplier);
  }

  /** {@inheritDoc} */
  @Override
  public void trace(CharSequence message, Throwable cause, Supplier<?>... paramsSupplier) {
    log(LEVEL_TRACE_INT, message, cause, paramsSupplier);
  }

  /** {@inheritDoc} */
  @Override
  public void trace(CharSequence message, Throwable cause, Object... params) {
    log(LEVEL_TRACE_INT, message, cause, params);
  }

  /** {@inheritDoc} */
  @Override
  public LoggingBuilder atTrace() {
    return new DefaultLoggingBuilder(this, LEVEL_TRACE_INT);
  }

  /** {@inheritDoc} */
  @Override
  public boolean isDebugEnable() {
    return isEnable(LEVEL_DEBUG_INT);
  }

  /** {@inheritDoc} */
  @Override
  public void debug(CharSequence message) {
    log(LEVEL_DEBUG_INT, message);
  }

  /** {@inheritDoc} */
  @Override
  public void debug(CharSequence message, Throwable cause) {
    log(LEVEL_DEBUG_INT, message, cause);
  }

  /** {@inheritDoc} */
  @Override
  public void debug(CharSequence message, Object... params) {
    log(LEVEL_DEBUG_INT, message, params);
  }

  /** {@inheritDoc} */
  @Override
  public void debug(CharSequence message, Supplier<?>... paramsSupplier) {
    log(LEVEL_DEBUG_INT, message, paramsSupplier);
  }

  /** {@inheritDoc} */
  @Override
  public void debug(CharSequence message, Throwable cause, Supplier<?>... paramsSupplier) {
    log(LEVEL_DEBUG_INT, message, cause, paramsSupplier);
  }

  /** {@inheritDoc} */
  @Override
  public void debug(CharSequence message, Throwable cause, Object... params) {
    log(LEVEL_DEBUG_INT, message, cause, params);
  }

  /** {@inheritDoc} */
  @Override
  public LoggingBuilder atDebug() {
    return new DefaultLoggingBuilder(this, LEVEL_DEBUG_INT);
  }

  /** {@inheritDoc} */
  @Override
  public boolean isInfoEnable() {
    return isEnable(LEVEL_INFO_INT);
  }

  /** {@inheritDoc} */
  @Override
  public void info(CharSequence message) {
    log(LEVEL_INFO_INT, message);
  }

  /** {@inheritDoc} */
  @Override
  public void info(CharSequence message, Throwable cause) {
    log(LEVEL_INFO_INT, message, cause);
  }

  /** {@inheritDoc} */
  @Override
  public void info(CharSequence message, Object... params) {
    log(LEVEL_INFO_INT, message, params);
  }

  /** {@inheritDoc} */
  @Override
  public void info(CharSequence message, Supplier<?>... paramsSupplier) {
    log(LEVEL_INFO_INT, message, paramsSupplier);
  }

  /** {@inheritDoc} */
  @Override
  public void info(CharSequence message, Throwable cause, Supplier<?>... paramsSupplier) {
    log(LEVEL_INFO_INT, message, cause, paramsSupplier);
  }

  /** {@inheritDoc} */
  @Override
  public void info(CharSequence message, Throwable cause, Object... params) {
    log(LEVEL_INFO_INT, message, cause, params);
  }

  /** {@inheritDoc} */
  @Override
  public LoggingBuilder atInfo() {
    return new DefaultLoggingBuilder(this, LEVEL_INFO_INT);
  }

  /** {@inheritDoc} */
  @Override
  public boolean isWarnEnable() {
    return isEnable(LEVEL_WARN_INT);
  }

  /** {@inheritDoc} */
  @Override
  public void warn(CharSequence message) {
    log(LEVEL_WARN_INT, message);
  }

  /** {@inheritDoc} */
  @Override
  public void warn(CharSequence message, Throwable cause) {
    log(LEVEL_WARN_INT, message, cause);
  }

  /** {@inheritDoc} */
  @Override
  public void warn(CharSequence message, Object... params) {
    log(LEVEL_WARN_INT, message, params);
  }

  /** {@inheritDoc} */
  @Override
  public void warn(CharSequence message, Supplier<?>... paramsSupplier) {
    log(LEVEL_WARN_INT, message, paramsSupplier);
  }

  /** {@inheritDoc} */
  @Override
  public void warn(CharSequence message, Throwable cause, Supplier<?>... paramsSupplier) {
    log(LEVEL_WARN_INT, message, cause, paramsSupplier);
  }

  /** {@inheritDoc} */
  @Override
  public void warn(CharSequence message, Throwable cause, Object... params) {
    log(LEVEL_WARN_INT, message, cause, params);
  }

  /** {@inheritDoc} */
  @Override
  public LoggingBuilder atWarn() {
    return new DefaultLoggingBuilder(this, LEVEL_WARN_INT);
  }

  /** {@inheritDoc} */
  @Override
  public boolean isErrorEnable() {
    return isEnable(LEVEL_ERROR_INT);
  }

  /** {@inheritDoc} */
  @Override
  public void error(CharSequence message) {
    log(LEVEL_ERROR_INT, message);
  }

  /** {@inheritDoc} */
  @Override
  public void error(CharSequence message, Throwable cause) {
    log(LEVEL_ERROR_INT, message, cause);
  }

  /** {@inheritDoc} */
  @Override
  public void error(CharSequence message, Object... params) {
    log(LEVEL_ERROR_INT, message, params);
  }

  /** {@inheritDoc} */
  @Override
  public void error(CharSequence message, Supplier<?>... paramsSupplier) {
    log(LEVEL_ERROR_INT, message, paramsSupplier);
  }

  /** {@inheritDoc} */
  @Override
  public void error(CharSequence message, Throwable cause, Supplier<?>... paramsSupplier) {
    log(LEVEL_ERROR_INT, message, cause, paramsSupplier);
  }

  /** {@inheritDoc} */
  @Override
  public void error(CharSequence message, Throwable cause, Object... params) {
    log(LEVEL_ERROR_INT, message, cause, params);
  }

  /** {@inheritDoc} */
  @Override
  public LoggingBuilder atError() {
    return new DefaultLoggingBuilder(this, LEVEL_ERROR_INT);
  }

  /** {@inheritDoc} */
  @Override
  public boolean isFatalEnable() {
    return isEnable(LEVEL_FATAL_INT);
  }

  /** {@inheritDoc} */
  @Override
  public void fatal(CharSequence message) {
    log(LEVEL_FATAL_INT, message);
  }

  /** {@inheritDoc} */
  @Override
  public void fatal(CharSequence message, Throwable cause) {
    log(LEVEL_FATAL_INT, message, cause);
  }

  /** {@inheritDoc} */
  @Override
  public void fatal(CharSequence message, Object... params) {
    log(LEVEL_FATAL_INT, message, params);
  }

  /** {@inheritDoc} */
  @Override
  public void fatal(CharSequence message, Supplier<?>... paramsSupplier) {
    log(LEVEL_FATAL_INT, message, paramsSupplier);
  }

  /** {@inheritDoc} */
  @Override
  public void fatal(CharSequence message, Throwable cause, Supplier<?>... paramsSupplier) {
    log(LEVEL_FATAL_INT, message, cause, paramsSupplier);
  }

  /** {@inheritDoc} */
  @Override
  public void fatal(CharSequence message, Throwable cause, Object... params) {
    log(LEVEL_FATAL_INT, message, cause, params);
  }

  /** {@inheritDoc} */
  @Override
  public LoggingBuilder atFatal() {
    return new DefaultLoggingBuilder(this, LEVEL_FATAL_INT);
  }
}
