/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.logging.event;

import java.util.ArrayList;
import java.util.function.Supplier;
import org.seppiko.commons.logging.Logging;
import org.seppiko.commons.logging.LoggingBuilder;

/**
 * Default logging builder implementation.
 *
 * @author Leonard Woo
 */
public class DefaultLoggingBuilder implements LoggingBuilder {

  private final Logging logging;
  private final int level;
  private CharSequence message;
  private Throwable cause;
  private final ArrayList<Object> params = new ArrayList<>();

  /**
   * Default logging builder constructor.
   *
   * @param logging delegate logging.
   * @param level logging level.
   */
  DefaultLoggingBuilder(Logging logging, int level) {
    this.logging = logging;
    this.level = level;
  }

  /** {@inheritDoc} */
  @Override
  public LoggingBuilder message(CharSequence message) {
    this.message = message;
    return this;
  }

  /** {@inheritDoc} */
  @Override
  public LoggingBuilder withCause(Throwable cause) {
    this.cause = cause;
    return this;
  }

  /** {@inheritDoc} */
  @Override
  public LoggingBuilder param(Supplier<?> paramSupplier) {
    params.add(paramSupplier.get());
    return this;
  }

  /** {@inheritDoc} */
  @Override
  public LoggingBuilder param(Object paramObject) {
    params.add(paramObject);
    return this;
  }

  /** {@inheritDoc} */
  @Override
  public void log() {
    Object[] objects = params.toArray(Object[]::new);
    logging.log(level, message, cause, objects);
  }
}
