/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.logging;

import java.security.PermissionCollection;
import java.security.PrivilegedAction;
import java.security.SecurityPermission;
import java.util.ArrayList;
import java.util.List;
import java.util.ServiceLoader;
import org.seppiko.commons.logging.event.DefaultLogging;
import org.seppiko.commons.logging.event.DefaultLoggingServiceProvider;
import org.seppiko.commons.logging.spi.LoggingServiceProvider;
import org.seppiko.commons.logging.utils.Report;

/**
 * Logging factory.
 *
 * @author Leonard Woo
 */
public class LoggingFactory {

  /** default provider */
  private static final DefaultLoggingServiceProvider DEFAULT_PROVIDER =
      new DefaultLoggingServiceProvider();

  /** provider */
  static volatile LoggingServiceProvider PROVIDER;

  /** Protected constructor that is not available for public use. */
  protected LoggingFactory() {}

  /**
   * Return a logger named according to the name parameter using the statically bound {@link
   * ILoggingFactory} instance.
   *
   * @param name the name of the logger.
   * @return log
   */
  public static Logging getLogging(String name) {
    return getILoggingFactory().getLogging(name);
  }

  /**
   * Return a logger named corresponding to the class passed as parameter, using the statically
   * bound {@link ILoggingFactory} instance.
   *
   * @param clazz the returned log will be named after clazz.
   * @return log
   */
  public static Logging getLogging(Class<?> clazz) {
    return getLogging(clazz.getName());
  }

  /**
   * Return the {@link ILoggingFactory} instance in use.
   *
   * @return the ILogFactory instance in use.
   */
  static ILoggingFactory getILoggingFactory() {
    if (PROVIDER == null) {
      synchronized (LoggingFactory.class) {
        if (PROVIDER == null) {
          bind();
        }
      }
    }

    if (PROVIDER != null) {
      return PROVIDER.getILoggingFactory();
    }

    Report.diag("[LOOKUP] Not found service provider.");
    Report.diag("[LOOKUP] Use default logging implementation.");
    return DEFAULT_PROVIDER.getILoggingFactory();
  }

  /** bind provider */
  private static void bind() {
    try {
      List<LoggingServiceProvider> providers =
          findServiceProvider(LoggingFactory.class.getClassLoader());
      if (providers.size() > 1) {
        Report.diag("Class path contains multiple service providers.");
        for (LoggingServiceProvider provider : providers) {
          Report.diag("Found provider [" + provider + "]");
        }
      }

      if (!providers.isEmpty()) {
        PROVIDER = providers.get(0);
        PROVIDER.initialize();

        if (providers.size() > 1) {
          Report.diag("Actual provider is of type [" + providers.get(0) + "].");
        }
      }

      synchronized (DEFAULT_PROVIDER) {
        for (DefaultLogging defaultLog : DEFAULT_PROVIDER.getLoggingFactory().getLoggingList()) {
          Logging log = getLogging(defaultLog.getName());
          defaultLog.setDelegate(log);
        }
      }

      DEFAULT_PROVIDER.getLoggingFactory().clear();
    } catch (Exception e) {
      PROVIDER = null;
      Report.diag("Failed to instantiate LoggingFactory.", e);
      throw new IllegalStateException("Unexpected initialization failure.", e);
    }
  }

  /**
   * find provider
   *
   * @return service provider list
   */
  private static List<LoggingServiceProvider> findServiceProvider(final ClassLoader classLoader) {
    List<LoggingServiceProvider> providers = new ArrayList<>();
    ServiceLoader<LoggingServiceProvider> loader = getServiceLoader(classLoader);
    loader.forEach(providers::add);
    return providers;
  }

  /**
   * get service loader
   *
   * @param classLoader factory loader
   * @return service loader
   */
  private static ServiceLoader<LoggingServiceProvider> getServiceLoader(ClassLoader classLoader) {
    ServiceLoader<LoggingServiceProvider> serviceLoader;
    PermissionCollection permissionCollection =
        new SecurityPermission("insertProvider").newPermissionCollection();
    if (permissionCollection == null) {
      serviceLoader = ServiceLoader.load(LoggingServiceProvider.class, classLoader);
    } else {
      final PrivilegedAction<ServiceLoader<LoggingServiceProvider>> action =
          () -> ServiceLoader.load(LoggingServiceProvider.class, classLoader);
      serviceLoader = action.run();
    }
    return serviceLoader;
  }
}
