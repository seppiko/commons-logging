/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.logging;

import java.util.function.Supplier;

/**
 * Logging interface.
 *
 * @author Leonard Woo
 */
public interface Logging {

  /** The {@code TRACE} level logging. */
  int LEVEL_TRACE_INT = 10;

  /** The {@code DEBUG} level logging. */
  int LEVEL_DEBUG_INT = 20;

  /** The {@code INFO} level logging. */
  int LEVEL_INFO_INT = 30;

  /** The {@code WARN} level logging. */
  int LEVEL_WARN_INT = 40;

  /** The {@code ERROR} level logging. */
  int LEVEL_ERROR_INT = 50;

  /** The {@code FATAL} level logging. */
  int LEVEL_FATAL_INT = 60;

  /**
   * Get logging name.
   *
   * @return logging name.
   */
  String getName();

  /**
   * Checks whether this Logging is enabled for the given Level.
   *
   * @param level the logger level to check.
   * @return if {@code true} this Logger is enabled for level, otherwise {@code false}.
   */
  boolean isEnable(int level);

  /**
   * Logs a message with the given level.
   *
   * @param level the logging level.
   * @param message the message string to be logged.
   */
  void log(int level, CharSequence message);

  /**
   * Logs a message with the given level.
   *
   * @param level the logging level.
   * @param message the message string to be logged.
   * @param cause A {@link Throwable} subclass or {@code null}.
   */
  void log(int level, CharSequence message, Throwable cause);

  /**
   * Logs a message with the given level.
   *
   * @param level the logging level.
   * @param message the message string to be logged.
   * @param paramSuppliers the parameter Suppliers or {@code null} to be logged.
   */
  void log(int level, CharSequence message, Supplier<?>... paramSuppliers);

  /**
   * Logs a message with the given level.
   *
   * @param level the logging level.
   * @param message the message string to be logged.
   * @param params the parameter objects or {@code null} to be logged.
   */
  void log(int level, CharSequence message, Object... params);

  /**
   * Logs a message with the given level.
   *
   * @param level the logging level.
   * @param message the message string to be logged.
   * @param cause a {@link Throwable} subclass or {@code null}.
   * @param paramSuppliers the parameter Suppliers or {@code null} to be logged.
   */
  void log(int level, CharSequence message, Throwable cause, Supplier<?>... paramSuppliers);

  /**
   * Logs a message with the given level.
   *
   * @param level the logging level.
   * @param message the message string to be logged.
   * @param cause a {@link Throwable} subclass or {@code null}.
   * @param params the parameter objects or {@code null} to be logged.
   */
  void log(int level, CharSequence message, Throwable cause, Object... params);

  /**
   * Checks whether this Logger is enabled for the TRACE level.
   *
   * @return true if this Logger is enabled for level TRACE, false otherwise.
   */
  boolean isTraceEnable();

  /**
   * Logs a message with the TRACE level.
   *
   * @param message the message object to log.
   */
  void trace(CharSequence message);

  /**
   * Logs a message at the TRACE level including the stack trace of the Throwable.
   *
   * @param message the message object to log.
   * @param cause the Throwable to log, including its stack trace.
   */
  void trace(CharSequence message, Throwable cause);

  /**
   * Logs a message at the TRACE level with parameters, including the stack trace of the Throwable.
   *
   * @param message the message object to log.
   * @param params the parameters to the message.
   */
  void trace(CharSequence message, Object... params);

  /**
   * Logs a message at the TRACE level with parameters, including the stack trace of the Throwable.
   *
   * @param message the message object to log.
   * @param paramsSupplier the parameter Suppliers to log.
   */
  void trace(CharSequence message, Supplier<?>... paramsSupplier);

  /**
   * Logs a message at the TRACE level with parameters, including the stack trace of the Throwable.
   *
   * @param message the message object to log.
   * @param cause the {@link Throwable} subclass to log, including its stack trace.
   * @param paramsSupplier the parameter Suppliers to log.
   */
  void trace(CharSequence message, Throwable cause, Supplier<?>... paramsSupplier);

  /**
   * Logs a message at the TRACE level with parameters, including the stack trace of the Throwable.
   *
   * @param message the message object to log.
   * @param cause the {@link Throwable} subclass to log, including its stack trace.
   * @param params the parameters to the message.
   */
  void trace(CharSequence message, Throwable cause, Object... params);

  /**
   * Level TRACE
   *
   * @return a new {@link LoggingBuilder} instance as appropriate for this log.
   */
  LoggingBuilder atTrace();

  /**
   * Checks whether this Logger is enabled for the DEBUG level.
   *
   * @return true if this Logger is enabled for level DEBUG, false otherwise.
   */
  boolean isDebugEnable();

  /**
   * Logs a message with the DEBUG level.
   *
   * @param message the message object to log.
   */
  void debug(CharSequence message);

  /**
   * Logs a message at the DEBUG level including the stack trace of the Throwable.
   *
   * @param message the message object to log.
   * @param cause the {@link Throwable} subclass to log, including its stack trace.
   */
  void debug(CharSequence message, Throwable cause);

  /**
   * Logs a message at the DEBUG level with parameters, including the stack trace of the Throwable.
   *
   * @param message the message object to log.
   * @param params the parameters to the message.
   */
  void debug(CharSequence message, Object... params);

  /**
   * Logs a message at the DEBUG level with parameters, including the stack trace of the Throwable.
   *
   * @param message the message object to log.
   * @param paramsSupplier the parameter Suppliers to log.
   */
  void debug(CharSequence message, Supplier<?>... paramsSupplier);

  /**
   * Logs a message at the DEBUG level with parameters, including the stack trace of the Throwable.
   *
   * @param message the message object to log.
   * @param cause the {@link Throwable} subclass to log, including its stack trace.
   * @param paramsSupplier the parameter Suppliers to log.
   */
  void debug(CharSequence message, Throwable cause, Supplier<?>... paramsSupplier);

  /**
   * Logs a message at the DEBUG level with parameters, including the stack trace of the Throwable.
   *
   * @param message the message object to log.
   * @param cause the {@link Throwable} subclass to log, including its stack trace.
   * @param params the parameters to the message.
   */
  void debug(CharSequence message, Throwable cause, Object... params);

  /**
   * Level DEBUG
   *
   * @return a new {@link LoggingBuilder} instance as appropriate for this log.
   */
  LoggingBuilder atDebug();

  /**
   * Checks whether this Logger is enabled for the INFO level.
   *
   * @return true if this Logger is enabled for level INFO, false otherwise.
   */
  boolean isInfoEnable();

  /**
   * Logs a message with the INFO level.
   *
   * @param message the message object to log.
   */
  void info(CharSequence message);

  /**
   * Logs a message at the INFO level including the stack trace of the Throwable. throwable passed
   * as parameter.
   *
   * @param message the message object to log.
   * @param cause the {@link Throwable} subclass to log, including its stack trace.
   */
  void info(CharSequence message, Throwable cause);

  /**
   * Logs a message at the INFO level with parameters, including the stack trace of the Throwable.
   *
   * @param message the message object to log.
   * @param params the parameters to the message.
   */
  void info(CharSequence message, Object... params);

  /**
   * Logs a message at the INFO level with parameters, including the stack trace of the Throwable.
   *
   * @param message the message object to log.
   * @param paramsSupplier the parameter Suppliers to log.
   */
  void info(CharSequence message, Supplier<?>... paramsSupplier);

  /**
   * Logs a message at the INFO level with parameters, including the stack trace of the Throwable.
   *
   * @param message the message object to log.
   * @param cause the {@link Throwable} subclass to log, including its stack trace.
   * @param paramsSupplier the parameter Suppliers to log.
   */
  void info(CharSequence message, Throwable cause, Supplier<?>... paramsSupplier);

  /**
   * Logs a message at the INFO level with parameters, including the stack trace of the Throwable.
   *
   * @param message the message object to log.
   * @param cause the {@link Throwable} subclass to log, including its stack trace.
   * @param params the parameters to the message.
   */
  void info(CharSequence message, Throwable cause, Object... params);

  /**
   * Level INFO
   *
   * @return a new {@link LoggingBuilder} instance as appropriate for this log.
   */
  LoggingBuilder atInfo();

  /**
   * Checks whether this Logger is enabled for the WARN level.
   *
   * @return true if this Logger is enabled for level WARN, false otherwise.
   */
  boolean isWarnEnable();

  /**
   * Logs a message with the WARN level.
   *
   * @param message the message object to log.
   */
  void warn(CharSequence message);

  /**
   * Logs a message at the WARN level including the stack trace of the Throwable. throwable passed
   * as parameter.
   *
   * @param message the message object to log.
   * @param cause the {@link Throwable} subclass to log, including its stack trace.
   */
  void warn(CharSequence message, Throwable cause);

  /**
   * Logs a message at the WARN level with parameters, including the stack trace of the Throwable.
   *
   * @param message the message object to log.
   * @param params the parameters to the message.
   */
  void warn(CharSequence message, Object... params);

  /**
   * Logs a message at the WARN level with parameters, including the stack trace of the Throwable.
   *
   * @param message the message object to log.
   * @param paramsSupplier the parameter Suppliers to log.
   */
  void warn(CharSequence message, Supplier<?>... paramsSupplier);

  /**
   * Logs a message at the WARN level with parameters, including the stack trace of the Throwable.
   *
   * @param message the message object to log.
   * @param cause the {@link Throwable} subclass to log, including its stack trace.
   * @param paramsSupplier the parameter Suppliers to log.
   */
  void warn(CharSequence message, Throwable cause, Supplier<?>... paramsSupplier);

  /**
   * Logs a message at the WARN level with parameters, including the stack trace of the Throwable.
   *
   * @param message the message object to log.
   * @param cause the {@link Throwable} subclass to log, including its stack trace.
   * @param params the parameters to the message.
   */
  void warn(CharSequence message, Throwable cause, Object... params);

  /**
   * Level WARN
   *
   * @return a new {@link LoggingBuilder} instance as appropriate for this log.
   */
  LoggingBuilder atWarn();

  /**
   * Checks whether this Logger is enabled for the ERROR level.
   *
   * @return true if this Logger is enabled for level ERROR, false otherwise.
   */
  boolean isErrorEnable();

  /**
   * Logs a message with the ERROR level.
   *
   * @param message the message object to log.
   */
  void error(CharSequence message);

  /**
   * Logs a message at the ERROR level including the stack trace of the Throwable. throwable passed
   * as parameter.
   *
   * @param message the message object to log.
   * @param cause the {@link Throwable} subclass to log, including its stack trace.
   */
  void error(CharSequence message, Throwable cause);

  /**
   * Logs a message at the ERROR level with parameters, including the stack trace of the Throwable.
   *
   * @param message the message object to log.
   * @param params the parameters to the message.
   */
  void error(CharSequence message, Object... params);

  /**
   * Logs a message at the ERROR level with parameters, including the stack trace of the Throwable.
   *
   * @param message the message object to log.
   * @param paramsSupplier the parameter Suppliers to log.
   */
  void error(CharSequence message, Supplier<?>... paramsSupplier);

  /**
   * Logs a message at the ERROR level with parameters, including the stack trace of the Throwable.
   *
   * @param message the message object to log.
   * @param cause the {@link Throwable} subclass to log, including its stack trace.
   * @param paramsSupplier the parameter Suppliers to log.
   */
  void error(CharSequence message, Throwable cause, Supplier<?>... paramsSupplier);

  /**
   * Logs a message at the ERROR level with parameters, including the stack trace of the Throwable.
   *
   * @param message the message object to log.
   * @param cause the {@link Throwable} subclass to log, including its stack trace.
   * @param params the parameters to the message.
   */
  void error(CharSequence message, Throwable cause, Object... params);

  /**
   * Level ERROR
   *
   * @return a new {@link LoggingBuilder} instance as appropriate for this log.
   */
  LoggingBuilder atError();

  /**
   * Checks whether this Logger is enabled for the FATAL level.
   *
   * @return true if this Logger is enabled for level FATAL, false otherwise.
   */
  boolean isFatalEnable();

  /**
   * Logs a message with the FATAL level.
   *
   * @param message the message object to log.
   */
  void fatal(CharSequence message);

  /**
   * Logs a message at the FATAL level including the stack trace of the Throwable. throwable passed
   * as parameter.
   *
   * @param message the message object to log.
   * @param cause the {@link Throwable} subclass to log, including its stack trace.
   */
  void fatal(CharSequence message, Throwable cause);

  /**
   * Logs a message at the FATAL level with parameters, including the stack trace of the Throwable.
   *
   * @param message the message object to log.
   * @param params the parameters to the message.
   */
  void fatal(CharSequence message, Object... params);

  /**
   * Logs a message at the FATAL level with parameters, including the stack trace of the Throwable.
   *
   * @param message the message object to log.
   * @param paramsSupplier the parameter Suppliers to log.
   */
  void fatal(CharSequence message, Supplier<?>... paramsSupplier);

  /**
   * Logs a message at the FATAL level with parameters, including the stack trace of the Throwable.
   *
   * @param message the message object to log.
   * @param cause the {@link Throwable} subclass to log, including its stack trace.
   * @param paramsSupplier the parameter Suppliers to log.
   */
  void fatal(CharSequence message, Throwable cause, Supplier<?>... paramsSupplier);

  /**
   * Logs a message at the FATAL level with parameters, including the stack trace of the Throwable.
   *
   * @param message the message object to log.
   * @param cause the {@link Throwable} subclass to log, including its stack trace.
   * @param params the parameters to the message.
   */
  void fatal(CharSequence message, Throwable cause, Object... params);

  /**
   * Level FATAL
   *
   * @return a new {@link LoggingBuilder} instance as appropriate for this log.
   */
  LoggingBuilder atFatal();
}
